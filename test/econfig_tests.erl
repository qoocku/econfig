-module(econfig_tests).
-compile (export_all).

-include ("proper/include/proper.hrl").
-include_lib ("eunit/include/eunit.hrl").

temp_dir () ->
  case os:type() of
    {unix, _} ->
      "/tmp";
    {_, _ } -> %% should be win32, but I do not know the exact value on windoze 64bit
      temp_dir_from_env(["TMP","TEMP"], false)
  end.

temp_dir_from_env ([], false) ->
  error(unknown_temp_dir);
temp_dir_from_env ([Env|Envs], false) ->
  temp_dir_from_env(Envs, os:getenv(Env));
temp_dir_from_env (_, Value) ->
  Value.

-define (MUT, econfig).

setup () ->
  ok   = application:set_env(econfig, data_dir, temp_dir()),
  application:start(gproc),
  application:start(econfig).
  
cleanup (_) ->
  application:stop(econfig).

-define (altosl(KeyPath), lists:map(fun atom_to_list/1, KeyPath)).

-type entries () :: {[atom()], integer()}.

entries_ () ->
  ?LET(Xs, {[atom()], integer()}, 
       begin
         random:seed(now()),
         {[list_to_atom("k"++integer_to_list(I)) || I <- lists:seq(1, random:uniform(10)+1)], random:uniform(1000)}
       end).

'set value property' () ->
  ?FORALL({KeyPath, Value}, entries_(),
          begin
            ?assertEqual(ok, ?MUT:set(xxx, KeyPath, Value)),
            ?assertMatch({ok, Value},
                         case application:get_env(xxx, hd(KeyPath)) of
                           undefined -> {ok, undefined};
                           {ok, Struct} ->
                             {ok, lists:foldl(fun 
                                                (K, S) when is_list(S) ->
                                                  proplists:get_value(K, S);
                                                (K, V) ->
                                                  V
                                              end, Struct, tl(KeyPath))}
                         end)
          end).

'test setting values' (_) ->
  proper:quickcheck('set value property'(), 500).

all_test_ () ->
  error_logger:tty(true),
  {foreach,
   fun setup/0,
   fun cleanup/1,
   [fun (Dir) ->
        fun () ->
            erlang:apply(?MODULE, F, [Dir])
        end
    end || {F, 1} <- ?MODULE:module_info(exports), lists:prefix("test ", atom_to_list(F))]}.


