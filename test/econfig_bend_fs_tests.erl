-module (econfig_bend_fs_tests).

-compile (export_all).

-include ("proper/include/proper.hrl").
-include_lib ("eunit/include/eunit.hrl").


temp_dir () ->
  case os:type() of
    {unix, _} ->
      "/tmp";
    {_, _ } -> %% should be win32, but I do not know the exact value on windoze 64bit
      temp_dir_from_env(["TMP","TEMP"], false)
  end.

temp_dir_from_env ([], false) ->
  error(unknown_temp_dir);
temp_dir_from_env ([Env|Envs], false) ->
  temp_dir_from_env(Envs, os:getenv(Env));
temp_dir_from_env (_, Value) ->
  Value.

-define (MUT, econfig_bend_fs).

setup () ->
  ok   = application:set_env(econfig, data_dir, temp_dir()),
  Expr = ?MUT:init(undefined),
  ?assertMatch({ok, Dir} when is_list(Dir), Expr),
  {ok, Dir} = Expr,
  Dir.
  
cleanup (_) ->
  ok.

-define (altosl(KeyPath), lists:map(fun atom_to_list/1, KeyPath)).

-type entries () :: {[atom()], integer()}.

entries_ () ->
  ?LET(Xs, {[atom()], integer()}, 
       begin
         random:seed(now()),
         {[list_to_atom("k"++integer_to_list(I)) || I <- lists:seq(1, random:uniform(2)+1)], random:uniform(1000)}
       end).

'set value property' (Dir) ->
  ?FORALL({KeyPath, Value}, entries_(),
          begin
            ?assertEqual(Dir, ?MUT:set_value([xxx|KeyPath], Value, Dir)),
            ?assertMatch({ok, [Value]},
                         file:consult(filename:join([Dir,"xxx"|?altosl(KeyPath)])))
          end).

'get value property' (Dir) ->
  ?FORALL({KeyPath, Value}, entries_(),
          begin
            ?assertEqual(ok, ?MUT:set_value([xxx|KeyPath], Value, Dir)),
            ?assertEqual(Value, ?MUT:get_value([xxx|KeyPath], Dir))
          end).

'test setting values' (Dir) ->
  proper:quickcheck('set value property'(Dir), 500).

'test getting values' (Dir) ->
  proper:quickcheck('get value property'(Dir), 500).

all_test_ () ->
  error_logger:tty(true),
  {foreach,
   fun setup/0,
   fun cleanup/1,
   [fun (Dir) ->
        fun () ->
            erlang:apply(?MODULE, F, [Dir])
        end
    end || {F, 1} <- ?MODULE:module_info(exports), lists:prefix("test ", atom_to_list(F))]}.
