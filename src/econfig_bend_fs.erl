%%% ===========================================================================
%% @doc The contents of this file are subject to the Erlang Public License,
%% Version 1.1, (the "License"); you may not use this file except in
%% compliance with the License. You should have received a copy of the
%% Erlang Public License along with this software. If not, it can be
%% retrieved via the world wide web at http://www.erlang.org/EPLICENSE.
%%
%% Software distributed under the License is distributed on an "AS IS"
%% basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
%% the License for the specific language governing rights and limitations
%% under the License.
%%
%% The Initial Developer of the Original Code is Ericsson Utvecklings AB.
%% Portions created by Ericsson are Copyright 1999, Ericsson Utvecklings
%% AB. All Rights Reserved.
%%
%% @author Damian Dobroczy\\'nski
%% @since 2012-09-13
%% @end
%%% ===========================================================================
-module(econfig_bend_fs).
-author ("Damian T. Dobroczy\\'nski <qoocku@gmail.com>").

-export ([init/1,
          get_all_values/1,
          get_all_values/2,
          get_value/2,
          set_value/3]).

-include_lib ("eunit/include/eunit.hrl").

-type ctx () :: file:name().
%% Context of this callback functions.
-type entries () :: [{atom(), any()} | {atom(), entries()}].
%% List of values mapped with keys.

-export_type ([entries/0]).

-spec init (any()) -> {ok, ctx()}.

init (_) ->
  {ok, Dir} = application:get_env(econfig, data_dir),
  {ok, Dir}.

-spec get_all_values (ctx()) -> entries().

get_all_values (Dir) when is_list(Dir) ->
  collect_entries(Dir, []).

-spec get_all_values (KeyPath::econfig:key_path(), ctx()) -> entries().

get_all_values ([Key|_], Dir) when is_atom(Key) andalso
                                   is_list(Dir) ->
  collect_entries(filename:join(Dir, erlang:atom_to_list(Key)), []).

-spec get_value (KeyPath::econfig:key_path(), ctx()) -> any().

get_value (Ks = [Key|_], Dir) when is_atom(Key) andalso
                                   is_list(Dir) ->
  Path = filename:join([erlang:atom_to_list(I) || I <- ([Dir] ++ Ks)]),
  {ok, [V]} = file:consult(Path),
  V.

-spec set_value (KeyPath::econfig:key_path(), any(), ctx()) -> ctx().

set_value (Ks = [Key|_], Value, Dir) when is_atom(Key) andalso
                                          is_list(Dir) ->
  Path    = ensure_leaf(Ks, Dir),
  {ok, F} = file:open(Path, [write]),
  try
    io:format(F, "~p.", [Value]),    
    Dir
  after
    file:close(F),
    Dir
  end.

%%% ================================ Local functions ==========================

%% @private
%% @doc Collects all entries saved in the filesystem.

collect_entries (Dir, Result) ->
  case filelib:is_dir(Dir) of
    true ->
      {ok, Filenames} = file:list_dir(Dir),
      lists:foldl(fun (File, Acc) ->
                      [{erlang:list_to_atom(File), collect_entries(filename:join(Dir, File), [])} | Acc]
                  end, Result, Filenames);
    false ->
      case filelib:is_regular(Dir) of
        true ->
          {ok, [Val]} = file:consult(Dir),
          Val;
        false ->
          []
      end
  end.
      
%% @private
%% @doc Ensures existence of a file.

ensure_leaf ([F], Dir) ->
  Path = filename:join(Dir, erlang:atom_to_list(F)),
  case filelib:is_dir(Path) of
    true ->
      ok = del_dir(Path);
    false ->
      ok
  end,
  Path;
ensure_leaf ([F|Fs], Dir) ->  
  Path = filename:join(Dir, erlang:atom_to_list(F)),
  case filelib:is_dir(Path) of
    true ->
      ensure_leaf(Fs, Path);
    false ->
      case filelib:is_regular(Path) of
        true ->
          ok = file:delete(Path),
          ensure_leaf([F|Fs], Dir);
        false ->
          ok = file:make_dir(Path),
          ensure_leaf(Fs, Path)
      end
  end.

del_dir (D) ->
  case filelib:is_dir(D) of
    true ->
      {ok, Filenames} = file:list_dir(D),
      lists:foreach(fun del_dir/1, [filename:join([D, F]) || F <- Filenames]),
      ok = file:del_dir(D);
    false ->
      case filelib:is_regular(D) of
        true ->
          ok = file:delete(D);
        false ->
          ok
      end
  end.



