%%% ===========================================================================
%% @doc The contents of this file are subject to the Erlang Public License,
%% Version 1.1, (the "License"); you may not use this file except in
%% compliance with the License. You should have received a copy of the
%% Erlang Public License along with this software. If not, it can be
%% retrieved via the world wide web at http://www.erlang.org/EPLICENSE.
%%
%% Software distributed under the License is distributed on an "AS IS"
%% basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
%% the License for the specific language governing rights and limitations
%% under the License.
%%
%% The Initial Developer of the Original Code is Ericsson Utvecklings AB.
%% Portions created by Ericsson are Copyright 1999, Ericsson Utvecklings
%% AB. All Rights Reserved.
%%
%% @author Damian Dobroczy\\'nski
%% @since 2012-09-13
%% @end
%%% ===========================================================================
-module(econfig_srv).
-author ("Damian T. Dobroczy\\'nski <qoocku@gmail.com>").
-behaviour(gen_server).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export ([init/1,
          handle_call/3,
          handle_info/2,
          terminate/2,
          code_change/3]).

-export ([update_structure/4]).

-include_lib ("eunit/include/eunit.hrl").

-record (state, {bend :: {module(), any()}}).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init (Args) ->
  {ok, BEnd} = application:get_env(econfig, data_backend),
  {ok, Data} = BEnd:init(Args),
  {ok, #state{bend = {BEnd, Data}}}.

handle_call ({all, App}, _From, State) ->
  {R, State1} = all(App, State),
  {reply, R, State1};
handle_call ({set, App, KeyPath, Value}, _From, State = #state{bend = {BEnd, Data}}) ->
  {reply, ok, State#state{bend={BEnd, txn(App, KeyPath, Value, BEnd, Data)}}};
handle_call ({delete, App, KeyPath}, _From, State) ->
  {reply, ok, delete(App, KeyPath, State)};
handle_call ({configure, App}, _From, State) ->
  {reply, ok, configure(App, State)}.

handle_info (_Info, State) ->
  {noreply, State}.

terminate (_Reason, _State) ->
  ok.

code_change (_OldVsn, State, _Extra) ->
  {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

all (App, State = #state{bend = {BEnd, Data}}) ->
  {BEnd:get_all_values([App], Data), State}.

%% @doc Merges existing config described in '*.app' and/or '*.config' file
%% with persistent structure stored locally. If `*.config.src' file is present
%% then the content its also merged with the following rules:
%% <ol>
%%  <li>if the source file has entry which is not included in the ordinary config:
%%      it is inserted to the ordinary file</li>
%%  <li>if the source file does not have an entry which exists on the ordinary config:
%%      it is removed from the ordinary config AND from the persistent cache.</li>
%%  <li>if the source file has an entry which exists in the ordinary config the
%%      decision about replacing ordinary entry with source entry depends on
%%      commands included in the source config file, in the `econfig' section of this file.
%%      The commands are given in form of list of tuples <code>{App, KeyPath, replace | '!replace'}</code>
%%      where `replace' effects in entry replacement iff cached value is the same as configured (or is not cached).
%%      <code>'!replace'</code> replaces unconditionally configured value and removes it form persistent cache.
%%  </li>
%% </ol>

configure (App, State = #state{bend = {BEnd, Data}}) ->
  Struct0 = application:get_all_env(App),
  Struct1 = BEnd:get_all_values([App], Data),
  Struct3 = merge_structures(Struct0, Struct1),
  lists:foreach(fun ({K, V}) -> application:set_env(App, K, V) end, Struct3),
  State.

delete (App, KeyPath, State = #state{bend = {BEnd, Data}}) ->
  error(not_implemented).

merge_structures (Into, From) ->
  lists:foldl(fun
                ({K, Vs=[{Atom, _}|_]}, Acc) when is_atom(Atom) ->
                  case lists:keyfind(K, 1, Acc) of
                    {K, Xs} ->
                      [{K, merge_structures(Xs, Vs)}|lists:keydelete(K, 1, Acc)];
                    false ->
                      [{K, Vs}|Acc]
                  end;
                ({K, V}, Acc) ->
                  [{K, V}|lists:keydelete(K, 1, Acc)]
              end, Into, From).

txn (App, Ks = [Key|Keys], Value, BEnd, Data) ->
  OldStruct = case application:get_env(App, Key) of
                {ok, Xs} -> Xs;
                undefined -> []
              end,
  try
    %% update the values structure
    NewStruct = update_structure(OldStruct, Keys, Value, []),
    application:set_env(App, Key, NewStruct),
    %% save the Value
    Data1 = save_value(App, Ks, Value, BEnd, Data),
    %% publish the change.
    gproc:send({p, l, {econfig, App}}, {econfig, App, Ks, Value}),
    erlang:put(was_success, true),
    Data1
  after
    %% restore previous Value if was an exception
    case erlang:get(was_success) of
      true ->
        erlang:erase(was_success);
      _ ->
        application:set_env(App, Key, OldStruct)
    end
  end.

save_value (App, Keys, Value, BEnd, Data) ->
  BEnd:set_value([App|Keys], Value, Data).

update_structure (_, [], Value, _) ->
  Value;
update_structure (List, [Key], Value, Result) when is_list(List) ->
  lists:foldl(fun ({K, Xs}, Acc) ->
                  [{K, Acc} | Xs]
              end, [{Key, Value} | lists:keydelete(Key, 1, List)],  Result);
update_structure (_Other, [Key], Value, Result) ->
  lists:foldl(fun ({K, Xs}, Acc) ->
                  [{K, Acc} | Xs]
              end, [{Key, Value}],  Result);
update_structure (List, [Key|Keys], Value, Result) ->
  case lists:keyfind(Key, 1, List) of
    {Key, Xs} when is_list(Xs) ->
      update_structure(Xs, Keys, Value, [{Key, lists:keydelete(Key, 1, List)}|Result]);
    _Other ->
      update_structure([], Keys, Value, [{Key, lists:keydelete(Key, 1, List)}|Result])
  end.
