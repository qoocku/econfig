%%% ===========================================================================
%%% @doc Public API for `econfig' application.
%%%
%%% == License ==
%%% The contents of this file are subject to the Erlang Public License,
%%% Version 1.1, (the "License"); you may not use this file except in
%%% compliance with the License. You should have received a copy of the
%%% Erlang Public License along with this software. If not, it can be
%%% retrieved via the world wide web at http://www.erlang.org/EPLICENSE.
%%%
%%% Software distributed under the License is distributed on an "AS IS"
%%% basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
%%% the License for the specific language governing rights and limitations
%%% under the License.
%%%
%%% @author Damian Dobroczy\\'nski
%%% @since 2012-09-13
%%% @end
%%% ===========================================================================
-module (econfig).
-author ("Damian T. Dobroczy\\'nski <qoocku@gmail.com>").

-export ([start/0,
          stop/0,
          start_link/1,
          configure/1,
          copy_from/2,
          delete/2,
          get/1,
          get/2,
          get/3,
          handle_info/3,
          set/2,
          set/3,
          subscribe/0,
          subscribe/1,
          struct/0,
          struct/1]).

-define (SERVER, econfig_srv).
-define (SUPERVISOR, econfig_sup).

%%% ===================================================================
%%% API functions
%%% ===================================================================

-type key_path () :: [atom()].
%% Path pointing to a configuration parameter.
-type change_info () :: {econfig, App::atom(), KeyPath::key_path(), Value::any()}.
%% Message published when a configuration parameter has been set.
-type type   () :: tree | integer | float | list | tuple | atom | boolean | string | binary.
%% Type of items in configuration.
-type struct_item () :: {Id::atom(), type(), struct() | integer() | float() | string() | atom() | tuple()}.
%% Desciption of one item in configuration. `Id' may be an application name or a parameter name.
-type struct () :: [struct_item()].
%% List of descriptions of configurations.


-export_type ([key_path/0,
               change_info/0,
               struct/0,
               type/0]).

-callback config_change (key_path(), any(), State::any()) -> NewState::any().
%% Callback template for all processes with behavior `econfig'.

%% @doc Starts this application.

start () ->
  application:start(?MODULE).

%% @doc Stops this application.

stop () ->
  application:stop(?MODULE).

%% @doc Starts one of main processes of this application.

-spec start_link (supervisor | worker) -> {ok, pid()}.

start_link (supervisor) ->
  supervisor:start_link({local, ?SUPERVISOR}, ?SUPERVISOR, []);
start_link (worker) ->
  gen_server:start_link({local, ?SERVER}, ?SERVER, [], []).

%% @doc Subscribes calling process onto this application subscription list.
%% The application processes publish {@link change_info(). message}
%% where `App' is id of an application for which `Value' has been set to the
%% parameter pointed by `KeyPath'.

-spec subscribe (atom()) -> ok.

subscribe (App) when is_atom(App) ->
  true = gproc:reg({p, l, {econfig, App}}),
  ok.

%% @doc Subscribes the calling process ont its application subscription list.

-spec subscribe () -> ok.

subscribe () ->
  {ok, App} = application:get_application(),
  subscribe(App).

%% @doc Updates/merge cached values with given application configuration.

-spec configure (atom()) -> ok.

configure (App) when is_atom(App) ->
  call({configure, App}).

%% @doc Deletes a value from configuration.

-spec delete (atom(), KeyPath::[atom()]) -> ok.

delete (App, KeyPath = [Key|_]) when is_atom(App) andalso
                                     is_atom(Key) ->
  call({delete, App, KeyPath}).

%% @doc Retrieves current application configuration value pointed by given key path.

-spec get (key_path()) -> any() | undefined.

get (Keys) ->
  {ok, App} = application:get_application(),
  ?MODULE:get(App, Keys).

%% @doc Retrieves a configuration value pointed by given key path.
%% If `X1' is an atom it denotes an application. `X1' may be a list
%% of atoms (key path) so then `X1' is the default value if the entry
%% is not found. Otherwise `X2' is a key path. In the latter case
%% an error is raised if the entry is not found.

-spec get (atom() | key_path(), key_path() | any()) -> any().

get (X1, X2 = [Key|_]) when is_atom(X1) andalso
                            is_atom(Key) ->
  get_value(X1, X2, fun () -> error(invalid_config) end);
get (X1 = [Key|_], X2) when is_atom(Key) ->
  {ok, App} = application:get_application(),
  ?MODULE:get(App, X1, X2).

%% @doc Retrieves a configuration value pointed by given key path.
%% If value does not exists it returns `Default'.

-spec get (atom(), key_path(), any()) -> any().

get (App, KeyPath, Default) when is_atom(App)     andalso
                                 is_list(KeyPath) andalso
                                 KeyPath =/= []->
  get_value(App, KeyPath, fun () -> Default end).

%% @doc Sets a configuration value pointed by given key path.

-spec set (atom(), key_path(), any()) -> ok.

set (App, KeyPath, Value) when is_atom(App)     andalso
                               is_list(KeyPath) andalso
                               KeyPath =/= [] ->
  call({set, App, KeyPath, Value}).

%% @doc Sets a confiuration value for current application.

-spec set (key_path(), any()) -> ok.

set (Keys, Value) ->
  {ok, App} = application:get_application(),
  ?MODULE:set(App, Keys, Value).

%% @doc Copies `App' configuration from `Node' node.

-spec copy_from (node(), atom()) -> ok.

copy_from (Node, App) ->
  Struct = gen_server:call({?SERVER, Node}, {all, App}),
  set_values(App, Struct).

%% @doc Utility for process handling configuration change.
%% It may be used in `gen_servers' like this:
%% ```
%% handle_info (...) ->
%%   ...;
%% handle_info (Info, State) ->
%%   {noreply, econfig:handle_info(Info, ?MODULE, State)}.
%% '''
%% or in `gen_event' handlers:
%% ```
%% handle_info (...) ->
%%   ...;
%% handle_info (Info, State) ->
%%   {ok, econfig:handle_info(Info, ?MODULE, State)}.
%% '''
%% @see subscribe/1
%% @see subscribe/0

-spec handle_info (change_info(), module(), any()) -> NewState::any().

handle_info ({econfig, App, Keys, Val}, Mod, State) ->
  case application:get_application() of
    {ok, App} ->
      Mod:config_change(Keys, Val, State);
    _ ->
      State
  end;
handle_info (_, _, State) ->
  State.

%% @doc Returns structure of configurations of applications.

-spec struct () -> struct().

struct () ->
  lists:foldl(fun ({App, _, _}, Acc) ->
                  [struct(App) | Acc]
              end, [], application:which_applications()).

%% @doc Returns structure of configuration of specific application.

-spec struct (atom()) -> struct_item().

struct (App) when is_atom(App) ->
  {App, tree, struct_items(lists:keydelete(included_applications, 1, application:get_all_env(App)))}.

%%% ========================== Local functions ========================

struct_items (Items = [{K, _}|_]) when is_atom(K) ->
  lists:foldl(fun({Id, Val}, Acc) ->
                  [{Id, struct_type(Val), struct_items(Val)}|Acc]
              end, [], Items);
struct_items (V) ->
  V.

struct_type (V) when V =:= true orelse V =:= false ->
  boolean;
struct_type (V) when is_atom(V) ->
  atom;
struct_type (V) when is_float(V) ->
  float;
struct_type (V) when is_integer(V) ->
  integer;
struct_type ([{A, _}|_]) when is_atom(A) ->
  tree;
struct_type (V) when is_list(V) ->
  string_or_list(V);
struct_type (V) when is_tuple(V) ->
  tuple;
struct_type (V) when is_binary(V) ->
  binary.

string_or_list (S) ->
  case lists:any(fun (C) -> C < 0 orelse C > 255 end, S) of
    true ->
      list;
    false ->
      string
  end.

-compile ({inline, [call/1, cast/1]}).

call (Msg) ->
  gen_server:call(?SERVER, Msg).

cast (Msg) ->
  gen_server:cast(?SERVER, Msg).

get_value (App, [Key], AbsentFun) when is_atom(App) ->
  case application:get_env(App, Key) of
    undefined ->
      AbsentFun();
    {ok, V} ->
      V
  end;
get_value (App, [Key|Keys], AbsentFun) when is_atom(App) ->
  case lists:foldl(fun
                     (_, {ok, undefined}) ->
                       AbsentFun();
                     (K, {ok, Struct}) ->
                       {ok, proplists:get_value(K, Struct)};
                     (_, undefined) ->
                       AbsentFun()
                   end, application:get_env(App, Key), Keys) of
    {ok, undefined} ->
      AbsentFun();
    Other ->
      Other
  end.

set_values (App, Struct) ->
  set_values(App, Struct, []).

set_values (App, [{Key, Struct}|Rest], Path) when is_list(Struct) ->
  ok = set_values(App, Struct, [Key|Path]),
  set_values(App, Rest, Path);
set_values (App, [{Key, Value}|Rest], Path) ->
  ok = set(App, lists:reverse([Key|Path]), Value),
  set_values(App, Rest, Path);
set_values (_App, [], _Path) ->
  ok.
