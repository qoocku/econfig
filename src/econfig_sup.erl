%%% ===========================================================================
%% @doc The contents of this file are subject to the Erlang Public License,
%% Version 1.1, (the "License"); you may not use this file except in
%% compliance with the License. You should have received a copy of the
%% Erlang Public License along with this software. If not, it can be
%% retrieved via the world wide web at http://www.erlang.org/EPLICENSE.
%%
%% Software distributed under the License is distributed on an "AS IS"
%% basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
%% the License for the specific language governing rights and limitations
%% under the License.
%%
%% The Initial Developer of the Original Code is Ericsson Utvecklings AB.
%% Portions created by Ericsson are Copyright 1999, Ericsson Utvecklings
%% AB. All Rights Reserved.
%%
%% @author Damian Dobroczy\\'nski
%% @since 2012-09-13
%% @end
%%% ===========================================================================
-module(econfig_sup).
-author ("Damian T. Dobroczy\\'nski <qoocku@gmail.com>").
-behaviour(supervisor).

%% Supervisor callbacks
-export ([init/1]).

%% Helper macro for declaring children of supervisor
-define (CHILD(I, S, Type), {I, S, permanent, 5000, Type, [I]}).


%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, {{one_for_one, 5, 10}, 
          [?CHILD(econfig_srv, {econfig, start_link, [worker]}, worker)]} }.

