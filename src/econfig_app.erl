%%% ===========================================================================
%% @doc The contents of this file are subject to the Erlang Public License,
%% Version 1.1, (the "License"); you may not use this file except in
%% compliance with the License. You should have received a copy of the
%% Erlang Public License along with this software. If not, it can be
%% retrieved via the world wide web at http://www.erlang.org/EPLICENSE.
%%
%% Software distributed under the License is distributed on an "AS IS"
%% basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
%% the License for the specific language governing rights and limitations
%% under the License.
%%
%% The Initial Developer of the Original Code is Ericsson Utvecklings AB.
%% Portions created by Ericsson are Copyright 1999, Ericsson Utvecklings
%% AB. All Rights Reserved.
%%
%% @author Damian Dobroczy\\'nski
%% @since 2012-09-13
%% @end
%%% ===========================================================================
-module (econfig_app).
-author ("Damian T. Dobroczy\\'nski <qoocku@gmail.com>").
-behaviour (application).

%% Application callbacks
-export ([start/2, 
          stop/1]).

%% ============================================================================
%% Application callbacks
%% ============================================================================

start (_StartType, _StartArgs) ->
  ok     = check_data_dir(),
  Result = {ok, _Pid} = econfig:start_link(supervisor),
  ok     = gather_command_line_args(),
  Result.

stop (_State) ->
  ok.

%%% =================== Local/internal functions ==============================

gather_command_line_args () ->
  case init:get_argument('-econfig') of
    {ok, Args} ->
      set_argument(Args);
    error ->
      ok
  end.

set_argument ([["set", AppStr, Path, Val]|Rest]) ->
  set_argument([[AppStr, Path, Val]|Rest], fun econfig:set/3);               
set_argument (Xs) ->
  set_argument(Xs, fun (App, Keys, RealValue) ->
                       OS = case application:get_env(App, hd(Keys)) of
                              {ok, Xs} -> Xs;
                              undefined -> []
                            end,
                       NS = econfig_srv:update_structure(OS, tl(Keys), RealValue, []),
                       ok = application:set_env(App, hd(Keys), NS)
                   end).

set_argument ([[AppStr, Path, Val]|Rest], SetFun) ->
  App       = list_to_atom(AppStr),
  Keys      = [list_to_atom(K) || K <- string:tokens(Path, "/")],
  RealValue = begin
                {ok, Exprs, _} = erl_scan:string(Val),
                {value, V, _}  = erl_eval:exprs(Exprs, erl_eval:new_bindings()),
                V
              end,
  SetFun(App, Keys, RealValue),
  set_argument(Rest);
set_argument ([], _) ->
  ok.

check_data_dir () ->
  {ok, Dir} = application:get_env(econfig, data_dir),
  ok        = filelib:ensure_dir(Dir),
  case {filelib:is_regular(Dir), filelib:is_dir(Dir)} of
    {true, _} ->
      {error, data_dir_is_a_regular_file};
    {false, false} ->
      file:make_dir(Dir);
    {false, true} ->
      ok;
    _Other ->
      {error, something_try_to_tell_me_monkeys_can_fly}
  end.
